//
//  PersonTableViewCell.swift
//  BMI calculator
//
//  Created by nguyen thanh vy on 30/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    //MARK: Properties
    
    @IBOutlet weak var infoNameLabel: UILabel!
    @IBOutlet weak var infoHeightLabel: UILabel!
    @IBOutlet weak var infoWeightLabel: UILabel!
    @IBOutlet weak var infoBMILabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
