//
//  PersonTableViewController.swift
//  BMI calculator
//
//  Created by nguyen thanh vy on 30/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//

import UIKit

class PersonTableViewController: UITableViewController {
    //MARK: Properties
     
    var people = [Person]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSampleData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return people.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PersonTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PersonTableViewCell.")
        }
        let person = people[indexPath.row]
        cell.infoNameLabel.text = person.name
        cell.infoWeightLabel.text = "Weight: \(String(person.weight))"
        cell.infoHeightLabel.text = "Height: \(String(person.height))"
        cell.infoBMILabel.text = "BMI: \(String(format: "%.1f",person.BMI))"

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Private Methods
     
    private func loadSampleData() {
        guard let person1 = Person(name: "Peter", weight: 50, height:160, age: 40, profession: ["teacher"]) else {
            fatalError("Unable to instantiate person1")
        }
        guard let person2 = Person(name: "Mary", weight: 45, height:175.5,  age: 20, profession: ["director, designer"]) else {
            fatalError("Unable to instantiate person1")
        }
        people += [person1, person2]
    }
    //MARK: Actions
    @IBAction func unwindToPersonList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? PersonViewController, let person = sourceViewController.person {
            // Add a new person.
            let newIndexPath = IndexPath(row: people.count, section: 0)
            people.append(person)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
}
