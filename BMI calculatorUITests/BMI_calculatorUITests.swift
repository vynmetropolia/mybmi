//
//  BMI_calculatorUITests.swift
//  BMI calculatorUITests
//
//  Created by nguyen thanh vy on 29/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//

import XCTest

class BMI_calculatorUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let tablesQuery = XCUIApplication().tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Peter"]/*[[".cells.staticTexts[\"Peter\"]",".staticTexts[\"Peter\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["BMI: 19.5"]/*[[".cells.staticTexts[\"BMI: 19.5\"]",".staticTexts[\"BMI: 19.5\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Weight: 50.0"]/*[[".cells.staticTexts[\"Weight: 50.0\"]",".staticTexts[\"Weight: 50.0\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Height: 160.0"]/*[[".cells.staticTexts[\"Height: 160.0\"]",".staticTexts[\"Height: 160.0\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Mary"]/*[[".cells.staticTexts[\"Mary\"]",".staticTexts[\"Mary\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["BMI: 14.6"]/*[[".cells.staticTexts[\"BMI: 14.6\"]",".staticTexts[\"BMI: 14.6\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Weight: 45.0"]/*[[".cells.staticTexts[\"Weight: 45.0\"]",".staticTexts[\"Weight: 45.0\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Height: 175.5"]/*[[".cells.staticTexts[\"Height: 175.5\"]",".staticTexts[\"Height: 175.5\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
                
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
